;;
;; Главный файл для загрузки всех переносимых настроек
;; Подключить в ~/.emacs как
;; (load-file "~/.emacs.d/lisp/init.el")
;;

;; путь для поиска и загрузки компонент
(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'load-path "~/.emacs.d/lisp/hatred")

; Server mode
; здесь, что бы сразу мог отработать emacsclient
(server-start)

(setq debug-on-error t)

;;
;;-----------------------------------------------------------------------------
;; Загрузка модулей
;;-----------------------------------------------------------------------------
;;


;;
;;==============================================================================
;; Settings by Alex Ott
;;==============================================================================
;;
(load-file "~/.emacs.d/lisp/alex-ott/emacs-rc-common-hooks.el")
;(load-file "~/.emacs.d/lisp/alex-ott/emacs-rc-cedet.el")
(load-file "~/.emacs.d/lisp/alex-ott/emacs-rc-auto-insert.el")
(load-file "~/.emacs.d/lisp/alex-ott/emacs-rc-cmake.el")
;(load-file "~/.emacs.d/lisp/alex-ott/emacs-rc-company-mode.el")


;;
;;==============================================================================
;; My own settings
;;==============================================================================
;;
(require 'hatred/melpa-rc)
(require 'hatred/misc-rc)
(require 'hatred/cua-rc)
(require 'hatred/maximize)
;(require 'hatred/auto-complete)
(require 'hatred/company-mode-rc)
(require 'hatred/flycheck-rc)
(require 'hatred/themes)
(require 'hatred/pair-brackets-rc)
(require 'hatred/ibuffer)
(require 'hatred/fill-column-indicator-rc)
(require 'hatred/emhacks-rc)
(require 'hatred/scrolling-rc)
(require 'hatred/org-rc)
(require 'hatred/deft-rc)
(require 'hatred/ispell-rc)
(require 'hatred/muse-rc)
(require 'hatred/asciidoc-rc)
(require 'hatred/cmake-ide-rc)
;(require 'hatred/lang/java-rc)  ; semantic is needed
(require 'hatred/lang/c_cpp-rc)
(require 'hatred/lang/lua-rc)
(require 'hatred/keys-global-rc) ; should be last


;;
;;-----------------------------------------------------------------------------
;; Установки, настройки
;;-----------------------------------------------------------------------------
;;

;;
;; Set font
;; В современных версиях лучше установить через меню: Options -> Set default font
;; и сохранить: Options -> Save options
;;
;(set-face-font 'default "9x15")
;;(set-face-font 'default "-misc-fixed-medium-r-normal--14-130-75-75-c-70-koi8-r")
;(set-face-font 'default "-outline-DejaVu Sans Mono-normal-r-normal-normal-*-*-96-96-c-*-iso10646-1")
;(set-default-font "-outline-DejaVu Sans Mono-normal-r-normal-normal-*-*-96-96-c-*-iso10646-1")
;(set-face-font 'default "DejaVu Sans Mono:size=12")
;(set-default-font "DejaVu Sans Mono:size=8")


;; Курсор - вертикальная полоска
; hbar - подчерк
; box  - дефолтный
; bar  - черта
(set-default 'cursor-type 'hbar)

;;
;; Faces
;;
;; gaz-work
;(when (string= (system-name) "gaz-work")
;  (set-face-attribute 'default nil
;                                :inherit nil
;                                :stipple nil
;                                :background "white"
;                                :foreground "black"
;                                :inverse-video nil
;                                :box nil
;                                :strike-through nil
;                                :overline nil
;                                :underline nil
;                                :slant 'normal
;                                :weight 'normal
;                                :height 110
;                                :width 'normal
;                                :foundry "unknown"
;                                :family "DejaVu Sans Mono")
;  (set-face-attribute 'default t
;                                :family "Liberation Mono"
;                                :foundry "unknown"
;                                :slant 'normal
;                                :weight 'normal
;                                :height 110
;                                :width 'normal)
;  (set-face-attribute 'tabbar-default nil
;                                :inherit nil
;                                :stipple nil
;                                :background "gray80"
;                                :foreground "black"
;                                :box nil
;                                :strike-through nil
;                                :underline nil
;                                :slant 'normal
;                                :weight 'normal
;                                :height 90
;                                :width 'normal
;                                :family "DejaVu Sans")
;  (set-face-attribute 'variable-pitch nil
;                                :height 100
;                                :family "DejaVu Sans")
;  )
;; gaz-eeepc
;(when (string= (system-name) "gaz_eeepc")
;  (set-face-attribute 'default nil
;                                :inherit nil
;                                :stipple nil
;                                :background "white"
;                                :foreground "black"
;                                :inverse-video nil
;                                :box nil
;                                :strike-through nil
;                                :overline nil
;                                :underline nil
;                                :slant 'normal
;                                :weight 'normal
;                                :height 80
;                                :width 'normal
;                                :foundry "unknown"
;                                :family "DejaVu Sans Mono")
;  (set-face-attribute 'tabbar-default nil
;                                :inherit nil
;                                :stipple nil
;                                :background "gray80"
;                                :foreground "black"
;                                :box nil
;                                :strike-through nil
;                                :underline nil
;                                :slant 'normal
;                                :weight 'normal
;                                :height 90
;                                :width 'normal
;                                :family "DejaVu Sans")
;  (set-face-attribute 'variable-pitch nil
;                                :height 80
;                                :family "DejaVu Sans")
;  )


(provide 'init)