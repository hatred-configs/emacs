#!/bin/sh

for i in *.el
do
  emacs -batch -q -f batch-byte-compile $i
done
