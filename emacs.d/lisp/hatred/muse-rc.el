;;
;; =============================
;; Muse
;; =============================
;;
;; http://mwolson.org/static/doc/muse.html
;; http://oramezo.org/ru/man/muse-howto
;;
(require 'muse-mode)
(require 'muse-html)
(require 'muse-colors)
(require 'muse-wiki)
(require 'muse-latex)
(require 'muse-texinfo)
(require 'muse-docbook)
(require 'muse-project)

(add-to-list 'auto-mode-alist '("\\.muse$" . muse-mode))

(defun my-muse-mode-hook ()
  (setq auto-fill-mode t)
  (flyspell-mode 1)
  (footnote-mode 1)
  (orgtbl-mode   1))
(add-hook 'muse-mode-hook 'my-muse-mode-hook)

; Стили
(muse-derive-style "my-page-html" "html"
                   :header "~/Documents/templates/muse/header.tmpl"
                   :footer "~/Documents/templates/muse/footer.tmpl")

(muse-derive-style "my-page-pdf" "pdf"
                   :header "~/Documents/templates/muse/header.tex"
                   :footer "~/Documents/templates/muse/footer.tex")

; Проект сайта
(setq muse-project-alist
      '(
        ("my-articles"
         (,@(muse-project-alist-dirs   "~/Documents/Articles/muse") :default "index")
          ,@(muse-project-alist-styles "~/Documents/Articles/muse"
                                       "~/Documents/Articles/muse"
                                       "xhtml1.1")
          (:base "my-page-pdf"
                :path "~/Documents/Articles/muse/pdf"))))

; Вспомогательная функция для генерации относительных путей
(defun muse-gen-relative-name (name)
  (concat
   (file-name-directory (muse-wiki-resolve-project-page (muse-project)))
   name))

; Gen change date
(defun generate-change-date (file)
    (when (file-exists-p file)
        (let* ((fa (file-attributes file))
            (mod-time (nth 6 fa)))
        (format-time-string "%d.%m.%Y %R" mod-time))))

; Автоопределение языка текущего файла
(defun muse-mp-detect-language ()
  (let ((lang "NN")
        (cur-dir (file-name-directory (muse-current-file)))
        )
    (let ((smatch (string-match "/\\(ru\\|en\\|de\\)/" cur-dir)))
      (when smatch
        (setq lang (substring cur-dir (+ smatch 1) (+ smatch 3)))))
    lang))


(provide 'hatred/muse-rc)
