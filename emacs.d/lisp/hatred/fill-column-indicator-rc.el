;;
;; Рисуем линию ограничивающую крайнюю колонку для редактирования
;; http://www.emacswiki.org/emacs/FillColumnIndicator
;;
(hatred/load-or-install-package 'fill-column-indicator)
(setq fci-rule-column 100)
(setq fci-rule-width  1)
;(setq fci-rule-color "darkblue")
(setq fcr-rule-character #x2502)
;(fci-rule-character-color "darkblue")
;(fci-always-use-textual-rule 't) ; всегда использовать текстовое представление линейки
;(add-hook 'emacs-startup-hook '(lambda () (fci-mode 1)))
;(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
;(global-fci-mode 1)



;
(provide 'hatred/fill-column-indicator-rc)