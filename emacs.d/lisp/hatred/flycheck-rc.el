(hatred/load-or-install-package 'flycheck)
(hatred/load-or-install-package 'flycheck-irony)

(global-flycheck-mode)

(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;; Need more clean solution
(setq irony-additional-clang-options '("-std=c++14"))

(provide 'hatred/flycheck-rc)
