;; Copyright (C) Alex Ott
;;
;; Author: Alex Ott <alexott@gmail.com>
;; Keywords:
;; Requirements:
;; Status: not intended to be distributed yet

(hatred/load-or-install-package 'company)

(setq company-backends '(company-clang company-cmake company-capf))

;; Global use
;(add-hook 'after-init-hook 'global-company-mode)
(add-hook 'after-init-hook 'global-company-mode)

;; Irony mode backend
(add-to-list 'load-path "~/.emacs.d/lisp/company-irony")
(add-to-list 'load-path "~/.emacs.d/lisp/irony-mode")
;(require 'company-irony)
(hatred/load-or-install-package 'company-irony)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

(provide 'hatred/company-mode-rc)