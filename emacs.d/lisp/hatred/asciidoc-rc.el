;;
;; Settings for AsciiDoc editing mode
;;
;; http://www.emacswiki.org/emacs/AsciiDoc
;; https://github.com/sensorflo/adoc-mode/wiki
;; https://github.com/metaperl/asciidoc-el

;(add-to-list 'load-path                 "~/.emacs.d/lisp/markup-faces")
;(add-to-list 'load-path                 "~/.emacs.d/lisp/adoc-mode")
;(add-to-list 'load-path                 "~/.emacs.d/lisp/doc-mode")
(add-to-list 'load-path                 "~/.emacs.d/lisp/asciidoc-el")

(hatred/load-or-install-package 'adoc-mode)
;(hatred/load-or-install-package 'doc-mode)

;(autoload 'adoc-mode "adoc-mode")
;(autoload 'doc-mode "doc-mode")

(add-to-list 'auto-mode-alist '("\\.adoc\\'" . adoc-mode))
;(add-to-list 'auto-mode-alist '("\\.adoc\\'" . doc-mode))

(add-hook 'adoc-mode-hook
        '(lambda ()
           ;(buffer-face-mode t)
           (turn-on-auto-fill)
           (hatred/load-or-install-package 'asciidoc)))

;(add-hook 'adoc-mode-hook (lambda() (buffer-face-mode t)))
;(add-hook 'adoc-mode-hook (lambda() (auto-fill-mode t)))
;(add-hook 'adoc-mode-hook (lambda() (require 'asciidoc)))

(provide 'hatred/asciidoc-rc)
