;;
;; Deft - Emacs quick notes
;;

;; http://jblevins.org/projects/deft/
(when (require 'deft nil 'noerror) 
   (setq
      deft-extension "org"
      deft-directory "~/documents/org/notes/"
      deft-text-mode 'org-mode)
   (global-set-key (kbd "<f12>") 'deft))


;
(provide 'hatred/deft-rc)