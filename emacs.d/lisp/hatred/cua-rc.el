;;
;; CUA mode settings
;;
;; ВНИМАНИЕ:
;; Если нет выделенного блока текста префикс C-x работает так же, как если бы
;; cua-mode не был включен. Если же есть выделение, то нужно использовать:
;; 1. C-x C-x
;; 2. S-C-x
;;
(cua-mode t)
(transient-mark-mode 1)
(setq cua-keep-region-after-copy t)

;
(provide 'hatred/cua-rc)