;;
;;==============================================================================
;; Misc modules
;;==============================================================================
;;


;; Configuration variables here:
;(setq semantic-load-turn-useful-things-on t)
;; Load CEDET
; убираем встроенный cedet
;(setq load-path
;      (remove (concat "/usr/share/emacs/"
;      (substring emacs-version 0 -2) "/lisp/cedet")
;      load-path))
;(load-file "/usr/share/emacs/site-lisp/cedet/common/cedet.el")
;(require 'semantic-ia)
;(require 'semantic-gcc)


;;
;; eproject
;;
(hatred/load-or-install-package 'eproject)

;;
;; Emacs IDE
;;
;(require 'eide)
;(eide-start)

; Load ECB
;(add-to-list 'load-path "/usr/share/emacs/site-lisp/ecb")
;(require 'ecb-autoloads)
;(require 'ecb)
(hatred/load-or-install-package 'ecb)
; WA: http://stackoverflow.com/questions/20129637/emacs-24-3-1-cedet-2-0-built-in-and-ecb-20131116-1319-errors-during-the-layou
(setq ecb-examples-bufferinfo-buffer-name nil)

;; Xrefactory
;(setq load-path (cons "~/.emacs.d/lisp/xref/emacs" load-path))
;(setq exec-path (cons "~/.emacs.d/lisp/xref" exec-path))
;(load "xrefactory")

;; Работа с Doxygen в Emacs:
;;   http://aur.archlinux.org/packages.php?ID=13854
;;   http://doxymacs.sourceforge.net/
;(require 'doxymacs)
(hatred/load-or-install-package 'doxymacs)

;;
;; Folding
;;
;(require 'folding)
;(autoload 'folding-mode "folding" "Folding mode" t)

;;
;; Do not pause on redisplay
;;
(setq redisplay-dont-pause t)

;;
;; Подсветка синтаксиса
;;
(require 'font-lock)
(setq font-lock-mode-maximum-decoration t)
(if (fboundp 'global-font-lock-mode)
    (global-font-lock-mode t))


;;
;; Что-то историческое...
;;
;(standard-display-8bit 128 255)

;; Conventional mouse/arrow movement & selection
;(require 'pc-select)  ; deprecated
;(pc-selection-mode)   ; deprecated
(delete-selection-mode t)


;;
;; Enable emacs functionality that is disabled by default
;;
(put 'eval-expression 'disabled nil)
(put 'set-goal-column 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'eval-expression 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(setq enable-recursive-minibuffers t)



;;
;; Misc
;;
(defconst query-replace-highlight t)       ; highlight during query
(defconst search-highlight t)              ; highlight incremental search
(setq inhibit-startup-message t)           ; no splash screen
(setq ls-lisp-dirs-first t)                ; display dirs first in dired
(setq ecb-tip-of-the-day nil)              ; turn off ECB tips
(recentf-mode 1)                           ; recently edited files in menu
(setq compilation-scroll-output 't)        ; scroll compilation buffer during output
(setq require-final-newline t)             ; в конце требуется перевод строки
(setq column-number-mode t)                ; показывать номера колонок
;(setq line-number-mode t)                  ; показывать номера строк
(global-linum-mode t)                      ; номера строк в колонке слева
;(display-time)
;(setq display-time-day-and-date t)
(set-default 'fill-column 100)
(tool-bar-mode nil)
(add-hook 'text-mode-hook 'turn-on-auto-fill)
(fset 'yes-or-no-p 'y-or-n-p)              ; делаем возможность отвечать y или n, вместо yes/no

; GUI Open File Dialog
;(defadvice find-file-read-args (around find-file-read-args-always-use-dialog-box act)
;  "Simulate invoking menu item as if by the mouse; see `use-dialog-box'."
;  (let ((last-nonmenu-event nil))
;    ad-do-it))

;;
;; Auto save and reopen desktop on startup
;; http://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Emacs-Sessions.html
;;
(desktop-save-mode 1)

;;
;; Save places
;; https://www.emacswiki.org/emacs/SavePlace
;;
(save-place-mode 1)


;;
;; Ivy Minibuffer completions
;; http://oremacs.com/swiper/
;;
;(hatred/load-or-install-package 'swiper)
;(hatred/load-or-install-package 'counsel)
;(ivy-mode 1)
;(setq ivy-use-virtual-buffers t)
;(setq ivy-count-format "(%d/%d) ")

; replace standard keybindings
;(global-set-key (kbd "C-s") 'swiper)
;(global-set-key (kbd "M-x") 'counsel-M-x)
;(global-set-key (kbd "C-x C-f") 'counsel-find-file)
;(global-set-key (kbd "<f1> f") 'counsel-describe-function)
;(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
;(global-set-key (kbd "<f1> l") 'counsel-load-library)
;(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
;(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
;;
;(global-set-key (kbd "C-c g") 'counsel-git)
;(global-set-key (kbd "C-c j") 'counsel-git-grep)
;(global-set-key (kbd "C-c k") 'counsel-ag)
;(global-set-key (kbd "C-x l") 'counsel-locate)
;(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
;;
;(global-set-key (kbd "C-c C-r") 'ivy-resume)

;;
;; IDO mode
;;
(ido-mode 1)
; make ido display choices vertically
(setq ido-separator "\n")
; display any item that contains the chars you typed
(setq ido-enable-flex-matching t)
(setq max-mini-window-height 0.5)


;
(provide 'hatred/misc-rc)
