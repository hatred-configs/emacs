;;
;; =====================================
;; C/C++
;; =====================================
;;

;;(setq tags-table-list '("./" "~/"))

;;
;; Template inserts
;;
(defun template-insert (file)
  (interactive)
  (insert-file (format "~/documents/templates/%s" file )))

;; Вставляем темплейт для функции
(defun template-insert-func-comment ()
  (interactive)
  (template-insert "func-comment.template"))

;;
;; Режим отображения пробельных символов
;; http://www.emacswiki.org/emacs/BlankMode
;; http://www.emacswiki.org/emacs/ShowWhiteSpace
;
;(require 'blank-mode)
;(blank-global-mode-on)
;(blank-mode)
;(add-hook 'font-lock-mode-hook 'blank-mode)
;
;(require 'whitespace)


;;
;; Code style formating
;;
(defun my-c-mode-common-hook ()
  (c-set-style "ellemtel")
  (setq-default indent-tabs-mode nil))

(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)
(add-hook 'c-mode-common-hook '(lambda() ;(auto-complete-mode   1)
                                         ;(fci-mode             1)
                                         ;(ede-minor-mode       nil)
                                         ;(company-mode)
                               )
)

(add-hook
 'c-mode-common-hook
 '(
   lambda()
     (define-key c-mode-map   [return] 'newline-and-indent)
     (define-key c++-mode-map [return] 'newline-and-indent)
     (define-key c-mode-map   "\e\ei"  'c-indent-line-or-region)
     (define-key c-mode-map   "\C-cu"  'uncomment-region)
     (define-key c-mode-map   "\e\efc" 'template-insert-func-comment)
     (define-key c-mode-map   "\e\eml" 'insert-mmc-log)
     )
 )

;(setq c-echo-syntactic-information-p t)

;;
;; Режим отображения внутри какой функции мы находимся
;;
;(which-function-mode)

;;
;; Отключаем создание резервных файлов
;;
(setq make-backup-files nil)

;;
;; Автозавершение кода clang'ом
;; http://kristianrumberg.wordpress.com/2010/04/22/smart-autocompletion-for-c-in-emacs/
;; https://github.com/Golevka/emacs-clang-complete-async
;(require 'auto-complete-clang-async)
;(require 'auto-complete-clang)
;(defun ac-cc-mode-setup ()
;  (setq clang-complete-executable "~/.emacs.d/clang-complete")
;  (setq ac-sources '(ac-source-clang-async))
;  ;(setq ac-sources '(ac-source-clang))
;  (launch-completion-proc)
;  (define-key c-mode-base-map (kbd "M-/") 'ac-complete-clang)
;)

;(defun my-ac-config ()
;  (add-hook 'c-mode-common-hook      'ac-cc-mode-setup)
;  (add-hook 'c++-mode-common-hook    'ac-cc-mode-setup)
  ;(add-hook 'auto-complete-mode-hook 'ac-common-setup)
  ;(global-auto-complete-mode t)
  ;(setq clang-completion-suppress-error 't)
;  )

;(my-ac-config)

;;
;; Irony mode (libclang based mode for C++/C)
;;
(hatred/load-or-install-package 'irony)

(add-hook 'c-mode-common-hook      'irony-mode)
(add-hook 'c++-mode-common-hook    'irony-mode)

; replace the `completion-at-point' and `complete-symbol' bindings in
; irony-mode's buffers by irony-mode's function
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;;
;; Настраиваем doxymacs
;;
; Включаем doxymacs для C/C++
(add-hook 'c-mode-common-hook 'doxymacs-mode)
; Включаем подстветку синтаксиса для Doxygen в модах C/C++
(defun my-doxymacs-font-lock-hook ()
        (if (or (eq major-mode 'c-mode) (eq major-mode 'c++-mode))
                (doxymacs-font-lock)))
(add-hook 'font-lock-mode-hook 'my-doxymacs-font-lock-hook)



;
(provide 'hatred/lang/c_cpp-rc)