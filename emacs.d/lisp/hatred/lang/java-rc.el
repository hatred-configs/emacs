;; =====================================
;; Java/JDEE
;; =====================================

; malabar-mode
;(add-to-list 'load-path "~/.emacs.d/malabar-mode/lisp")
;(require 'malabar-mode)
(hatred/load-or-install-package 'malabar-mode)
(setq malabar-groovy-lib-dir "~/.emacs.d/malabar-mode/lib")
(add-to-list 'auto-mode-alist '("\\.java\\'" . malabar-mode))
(add-to-list 'auto-mode-alist '("\\.bsh\\'" . malabar-mode))
(add-hook
 'malabar-mode-hook
 '(
   lambda()
         (define-key malabar-mode-map  [return] 'newline-and-indent)
         (define-key malabar-mode-map  "\e\ei"  'c-indent-line-or-region)
         (define-key malabar-mode-map  "\C-cu"  'uncomment-region)
         (add-hook 'after-save-hook 'malabar-compile-file-silently nil t)))

; Emacs elib
;(setq load-path (append (list "/usr/share/emacs/site-lisp/elib")
;                         load-path))
; jdee
;(add-to-list 'load-path (expand-file-name "/usr/share/emacs/site-lisp/jde/lisp"))
;(setq defer-loading-jde nil)
;(if defer-loading-jde
;    (progn
;      (autoload 'jde-mode "jde" "JDE mode." t)
;      (add-to-list 'auto-mode-alist '("\.java$" . jde-mode))
;      (add-to-list 'auto-mode-alist '("\.bsh$"  . jde-mode)))
;  (require 'jde))
;(add-hook
; 'jde-mode-hook
; '(
;   lambda()
;         (define-key jde-mode-map  [return] 'newline-and-indent)
;         (define-key jde-mode-map  "\e\ei"  'c-indent-line-or-region)
;         (define-key jde-mode-map  "\C-cu"  'uncomment-region)))



;
(provide 'hatred/lang/java-rc)