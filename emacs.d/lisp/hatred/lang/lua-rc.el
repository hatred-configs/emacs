;;
;; =============================
;; LUA
;; =============================
;;
; http://www.emacswiki.org/emacs/LuaMode
; http://www.enyo.de/fw/software/lua-emacs/lua2-mode.html
; common
(require 'lua-mode)
(require 'lua2-mode)
(setq auto-mode-alist (cons '("\\.lua$" . lua-mode) auto-mode-alist))
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-hook
 'lua-mode-hook
 '(lambda()
    (define-key lua-mode-map   [return] 'newline-and-indent)
    (define-key lua-mode-map   "\e\ei"  'c-indent-line-or-region)
    (define-key lua-mode-map   "\C-cu"  'uncomment-region)
    )
 )
; flymake
(require 'flymake-lua)
(add-hook 'lua-mode-hook 'flymake-lua-load)
; lua-block
(require 'lua-block)
(lua-block-mode t)
(setq lua-block-highlight-toggle 'overlay)
;(setq lua-block-highlight-toggle 'minibuffer)
;(setq lua-block-highlight-toggle t)






;
(provide 'hatred/lang/lua-rc)