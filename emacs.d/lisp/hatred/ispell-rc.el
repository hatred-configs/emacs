(require 'ispell)

;; Источник: http://habrahabr.ru/post/215055/
;; Src2:     http://qoyllur.github.io/blog/2013/05/29/provierka-orfoghrafii-v-emacs-anghliiskii-i-russkii/

;; список используемых нами словарей
(setq ispell-local-dictionary-alist
    '(("russian"
       "[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
       "[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
       "[-]"  nil ("-d" "ru_RU") nil utf-8)
      ("english"
       "[A-Za-z]" "[^A-Za-z]"
       "[']"  nil ("-d" "en_US") nil iso-8859-1)))

;; вместо aspell использовать hunspell
(setq ispell-really-aspell nil
      ispell-really-hunspell t)

;; полный путь к нашему пропатченному hunspell
(setq ispell-program-name "hunspell")

(provide 'hatred/ispell-rc)