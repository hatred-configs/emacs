;;
;; IBuffer configuration
;;

; Use IBuffer instead standart buffer switcher
(global-set-key (kbd "C-x C-b") 'ibuffer)



(provide 'hatred/ibuffer)