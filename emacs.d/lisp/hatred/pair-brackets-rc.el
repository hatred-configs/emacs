    ;;
;;
;; Настройки для парных скобок: подстветка, автоподстановка и т.п.
;;
;;


;;
;; Поддсветка парных скобок
;;
; http://www.emacswiki.org/cgi-bin/wiki/MicParen
;
(hatred/load-or-install-package 'mic-paren)
; sexp-mode - режим подстветки всего контекста внутри парных скобок
(setq paren-sexp-mode nil)
(paren-activate)
;
; http://www.hut.fi/u/rsaikkon/software/elisp/cparen.el
;
;(require 'cparen)
;(cparen-activate)
;(show-paren-mode t)

;;
;; AutoPair - автоматические парные скобки и кавычки
;; с версии Emacs 24.4 в комплекте идёт electric-pair-mode, автор рекомендует использовать его
;;  http://emacs-fu.blogspot.com/2010/06/automatic-pairing-of-brackets-and.html
;;  https://github.com/capitaomorte/autopair
;;

;; pre Emacs 24.4
;(add-to-list 'load-path "~/.emacs.d/lisp/autopair-git") ;; comment if autopair.el is in standard load path 
;(require 'autopair)
;(hatred/load-or-install-package 'autopair)
;(autopair-global-mode) ;; enable autopair in all buffers
;(setq autopair-autowrap t)

;; post Emacs 24.4, electric-pair-mode
(electric-pair-mode 1) ;; enable pair mode in all buffers

;; Delete selection mode compat
(put 'autopair-insert-opening 'delete-selection t)
(put 'autopair-skip-close-maybe 'delete-selection t)
(put 'autopair-insert-or-skip-quote 'delete-selection t)
(put 'autopair-extra-insert-opening 'delete-selection t)
(put 'autopair-extra-skip-close-maybe 'delete-selection t)
(put 'autopair-backspace 'delete-selection 'supersede)
(put 'autopair-newline 'delete-selection t)


(provide 'hatred/pair-brackets-rc)
