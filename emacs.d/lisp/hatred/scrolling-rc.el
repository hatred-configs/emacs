;;
;; My favourite scrolling
;;  http://emacs-fu.blogspot.com/2009/12/scrolling.html
;;
;(set-default 'truncate-lines t)
;(setq scroll-up-agressively 2)
;(setq scroll-down-agressively 2)
;(setq scroll-conservatively 50)
;(setq scroll-conservatively 10000)
;(setq scroll-preserve-screen-position 't)
;(setq scroll-margin 3)
(setq scroll-margin 1)
(setq scroll-step 1)
;(setq scroll-step           1
;      scroll-conservatively 10000)

;; Vertical Fractional Scrolling: http://www.gnu.org/software/emacs/manual/html_node/elisp/Vertical-Scrolling.html
; If this variable is non-nil, the line-move, scroll-up, and scroll-down functions will automatically modify the vertical scroll position to scroll through display rows that are taller than the height of the window, for example in the presence of large images. 
(setq auto-window-vscroll 1)

; Скролировать окно под курсором мыши (без передачи фокуса)
(setq mouse-wheel-follow-mouse 't)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 5))) ;; one line at a time
(setq mouse-wheel-progressive-speed 't)
(mouse-wheel-mode 1)
;
; http://www.emacswiki.org/emacs/SmoothScrolling
;
(require 'smooth-scroll)
(smooth-scroll-mode t)

; Settings from: http://zhangda.wordpress.com/2009/05/21/customize-emacs-automatic-scrolling-and-stop-the-cursor-from-jumping-around-as-i-move-it/
(setq scroll-margin 1
      scroll-conservatively 0
      scroll-up-aggressively 0.01
      scroll-down-aggressively 0.01)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; http://www.randomsample.de/dru5/node/25
(defun DE-visual-scroll-up (&optional arg)
  (interactive)
  (if (pos-visible-in-window-p (point-max))
      (message "End of buffer")
    (unless arg
      (setq arg 1))
    (let ((cur (point))
  pos visible)
      (setq pos
    (save-excursion
      (while (and (search-forward "\n" nil t)
  (= (length (pos-visible-in-window-p 
      (point) nil t)) 2)))
      (1- (point))))
      (setq visible
    (pos-visible-in-window-p pos nil t))
      ;; if point is fully visible, we can go there
      (when (and (= (length visible) 2)
 (not (= pos cur)))
(goto-char pos))
      ;; if point is partly visible, we only go there if we absolutely
      ;; have to (point is already at the top)
      (when (and (= pos cur) 
 (null (pos-visible-in-window-p (1- (point)))))
(forward-line 1))
      (set-window-vscroll nil (+ (window-vscroll) arg)))))

(defun DE-visual-scroll-down (&optional arg)
  (interactive)
  (if (pos-visible-in-window-p (point-min))
      (message "Beginning of buffer")
    (unless arg
      (setq arg 1))
    (let ((cur (point))
  pos visible)
      (setq pos
    (save-excursion
      (while (and (search-backward "\n" nil t)
(= (length (pos-visible-in-window-p (point) nil t)) 2)))
      (+ 1 (point))))
      (setq visible
    (pos-visible-in-window-p pos nil t))
      (when (and (= (length visible) 2)
 (not (= pos cur)))
(goto-char pos))
      (when (and (= pos cur) 
 (null (pos-visible-in-window-p 
(save-excursion (forward-line 1) (point)))))
(goto-char (1- (point))))
      (when (zerop (window-vscroll))
(message "vscroll is 0. Reverting to scroll-down.")
(scroll-down arg))
      (set-window-vscroll nil (- (window-vscroll) arg)))))


(defun DE-summary-scroll-up (arg)
  (interactive "p")
  (unless (DE-scroll-article-window 'DE-visual-scroll-up arg)
    (gnus-summary-scroll-up arg)))

(defun DE-summary-scroll-down (arg)
  (interactive "p")
  (unless (DE-scroll-article-window 'DE-visual-scroll-down arg)
    (gnus-summary-scroll-down arg)))

(defun DE-summary-next-page (&optional arg circular stop)
  (interactive "P")
  (unless (DE-scroll-article-window 'scroll-up nil)
    (gnus-summary-next-page arg circular stop)))

(defun DE-summary-prev-page (&optional arg move)
  (interactive "P")
  (unless (DE-scroll-article-window 'scroll-down nil)
    (gnus-summary-prev-page arg move)))

(defun DE-scroll-article-window (func arg)
  (if (and (eq (cdr gnus-article-current) (gnus-summary-article-number))
   (gnus-buffer-live-p gnus-article-buffer))
      (progn
;; special treatment for RSS with W3M
(if (and (eq (car gnus-current-select-method) 'nnrss)
 (not (get-buffer-window gnus-article-buffer)))
    (let (flag)
      (mapc (lambda (buf)
      (when (get-buffer-window buf)
(with-selected-window (get-buffer-window buf)
  (funcall func arg))
(setq flag t)))
    (w3m-list-buffers))
    flag)
  (with-selected-window (get-buffer-window gnus-article-buffer)
    (funcall func arg))
  t))
    (when (and (gnus-buffer-live-p gnus-article-buffer)
       (get-buffer-window gnus-article-buffer))
      ;; reset vscroll
      (set-window-vscroll (get-buffer-window gnus-article-buffer) 0))
    nil))

;(global-set-key (kbd "C-<down>") 'DE-visual-scroll-up)
;(global-set-key (kbd "C-<up>")   'DE-visual-scroll-down)
;(global-set-key [mouse-5] 'DE-visual-scroll-up)
;(global-set-key [mouse-4] 'DE-visual-scroll-down)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; http://www.emacswiki.org/emacs/vertical-consistency.el
(require 'vertical-consistency)
(global-set-key [mouse-4]'vert-backward-scroll)
(global-set-key [mouse-5]'vert-forward-scroll)
(global-set-key [mode-line mouse-4] 'vert-backward-scroll)
(global-set-key [mode-line mouse-5] 'vert-forward-scroll)
(global-set-key [(up)]'vert-previous-line)
(global-set-key [(down)]'vert-next-line)
(global-set-key [(prior)]'vert-backward-scroll)
(global-set-key [(next)]'vert-forward-scroll)
;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun my-scroll(arg)
    (setq current-pos (window-vscroll))
    (setq pos (+ current-pos arg))
    (set-window-vscroll (selected-window) pos)
    (message "POS: %f / %f; win beg: %f, win end: %f"
	pos
	(window-vscroll)
	(window-start (selected-window))
	(window-end (selected-window))
	)
)

(defun my-scroll-up()
    (interactive)
    (my-scroll 0.5)
)

(defun my-scroll-down()
    (interactive)
    (my-scroll -0.5)
)


;(global-set-key [mouse-4] 'my-scroll-up)
;(global-set-key [mouse-5] 'my-scroll-down)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; http://zwell.net/content/emacs.html
(defun smooth-scroll (increment)
(scroll-up increment) (sit-for 0.05)
(scroll-up increment) (sit-for 0.02)
(scroll-up increment) (sit-for 0.02)
(scroll-up increment) (sit-for 0.05)
(scroll-up increment) (sit-for 0.06)
(scroll-up increment))

;(global-set-key [(mouse-5)] '(lambda () (interactive) (smooth-scroll 1)))
;(global-set-key [(mouse-4)] '(lambda () (interactive) (smooth-scroll -1)))





(provide 'hatred/scrolling-rc)
