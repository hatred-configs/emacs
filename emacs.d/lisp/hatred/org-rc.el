;;
;; org-mode
;; http://www.emacswiki.org/emacs/OrgMode
;; http://habrahabr.ru/blogs/soft/105300/
;; http://habrahabr.ru/blogs/soft/28098/
;; http://habrahabr.ru/blogs/soft/63424/
;;
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(setq org-CUA-compatible t)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(setq org-replace-disputed-keys t)
(setq org-support-shift-select t)

(setq org-todo-keywords
           '((sequence "TODO(t)" "|" "DONE(d)" "CANCELED(c)")
             (sequence "TASK(k)" "NEXT(n)" "STARTED(s)" "WAIT(w)" "|" "DONE(d)" "CANCELED(c)")
             ;(sequence "REPORT(r)" "BUG(b)" "FEATURE(f)" "|" "FIXED")
             (sequence "MAYBE(m)" "|" "CANCELED(c)")
             (sequence "|" "CANCELED(c)")))

; Colors in Emacs: http://raebear.net/comp/emacscolors.html 
(setq org-todo-keyword-faces
  '(("TODO"     . (:foreground "DarkOrange1"          :weight bold))
    ("TASK"     . (:foreground "CornflowerBlue"          :weight bold))
    ("MAYBE"    . (:foreground "sea green"          :weight bold))
    ("WAIT"     . (:foreground "orange"       :weight bold))
    ("NEXT"     . (:foreground "cyan"       :weight bold))
    ;("DONE"     . (:foreground "green"        :weight bold))
    ("CANCELED" . (:foreground "purple" :weight bold))))

; agenta files
(setq org-agenda-files (list "~/Documents/org/default.org"
                             "~/Documents/org/work.org"
                             "~/Documents/org/home.org"
                             "~/Documents/org/books.org"))
(defun org-my()
  (interactive)
  (find-file "~/Documents/org/default.org"))

(add-hook
 'org-mode-hook
 '(
   lambda()
         (define-key org-mode-map  [return] 'newline-and-indent)
         (define-key org-mode-map  "\e\ei"  'c-indent-line-or-region)
         (define-key org-mode-map  "\C-ci"  'org-toggle-inline-images) ; toggle inline images
  )
)


;;
;; Org Babel
;;
(require 'ob-tangle)
(setq org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")
;(setq org-plantuml-jar-path "~/java/plantuml.jar")

(defun bh/display-inline-images ()
  (condition-case nil
      (org-display-inline-images)
    (error nil)))
(add-hook 'org-babel-after-execute-hook 'bh/display-inline-images 'append)


; Make babel results blocks lowercase
(setq org-babel-results-keyword "results")


(org-babel-do-load-languages
 (quote org-babel-load-languages)
 (quote ((emacs-lisp . t)
         (dot . t)
         (ditaa . t)
         (R . t)
         (python . t)
         (ruby . t)
         (gnuplot . t)
         (clojure . t)
         (sh . t)
         (ledger . t)
         (org . t)
         (plantuml . t)
         (latex . t))))

; Do not prompt to confirm evaluation
; This may be dangerous - make sure you understand the consequences
; of setting this -- see the docstring for details
(setq org-confirm-babel-evaluate nil)

; Use fundamental mode when editing plantuml blocks with C-c '
;;(add-to-list 'org-src-lang-modes (quote ("plantuml" . fundamental)))

;; Don't enable this because it breaks access to emacs from my Android phone
;(setq org-startup-with-inline-images nil)

;
(provide 'hatred/org-rc)
