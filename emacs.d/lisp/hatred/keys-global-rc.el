;;
;;-----------------------------------------------------------------------------
;; Global keys binding and rebindings
;;-----------------------------------------------------------------------------
;;

;;
;; Вставляем дату-время (man strftime)
;;
(defun insert-date ()
(interactive)
(insert (format-time-string "%a %d %b %Y %T" (current-time)))
)
(global-set-key (kbd "\e\et") 'insert-date)


;;
;; Удаление области. В включенными режимами "выделение с шифтом" и классическими
;; клавишами управления копирование/вырезкой/вставкой - не очень нужно.
;;
;(global-set-key (kbd "\e\ebc") 'delete-region)


;;
;; Esc-Esc-l - Переход на указанную строку
;;
(global-set-key (kbd "\e\el") 'goto-line)
(global-set-key [?\C-l] 'goto-line)

;;
;; C-RET - newline and indent text (Дефолтно: C-j)
;;
(global-set-key [\C-return] 'newline-and-indent)

;;
;; Поиск
;;
(global-set-key [?\C-\M-f] 'nonincremental-search-forward)
(global-set-key [?\C-\M-.] 'nonincremental-repeat-search-forward)
(global-set-key [?\C-\M-,] 'nonincremental-repeat-search-backward)

;;
;; MM-b k закрываем текущий буфер
;;
(global-set-key (kbd "\e\ebk") 'kill-this-buffer)
(global-set-key [?\C-w] 'kill-this-buffer)

;;
;; Open file
;; TODO
;;
(defun gui-file-open ()
  "GUI based file opening"
  (interactive)

(defadvice find-file-read-args (around find-file-read-args-always-use-dialog-box act)
  "Simulate invoking menu item as if by the mouse; see `use-dialog-box'."
  (let ((last-nonmenu-event nil))
    ad-do-it))
(find-file-read-args "Open File: " nil)
)
(global-set-key [?\C-o] 'gui-file-open)



;;
;; Smart-HOME
;;   First Home press - go to first column with non-space char
;;   Second Home press - go to first column
;;
(defun smart-beginning-of-line ()
  "Forces the cursor to jump to the first none whitespace char of the current line when pressing HOME"
  (interactive)
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line))))
(put 'smart-beginning-of-line 'CUA 'move)
(global-set-key [home] 'smart-beginning-of-line)


;;
;; Tabbar mode: переключение между табами
;;
;(global-set-key [(control shift tab)] 'tabbar-backward)
;(global-set-key [(control tab)]       'tabbar-forward)
(global-set-key [M-left]               'tabbar-backward)
(global-set-key [M-right]              'tabbar-forward)
; Дополнительные комбинаци,т.к. предыдущие переопределяются в org mode
(global-set-key [C-prior]              'tabbar-backward) ; Ctrl + PgUp
(global-set-key [C-next]               'tabbar-forward)  ; Ctrl + PgDown


;;
;; Scroll window without cursor moving (while cursor is visible)
;;
;(global-set-key [(control  down)]  'scroll-up-1)
;(global-set-key [(control  up)]    'scroll-down-1)
;(global-set-key [(control  left)]  'scroll-right-1)
;(global-set-key [(control  right)] 'scroll-left-1)


;;
;; Narrowing region
;; Полезная штука, когда нужно поработать с блоком текста, и гарантировать,
;; что соседние строки не будут тронуты
;;   Дефолтная комбинация: \C-x n n - для блока
;;                         \C-x n w - для возврата
;; Если включен CUA, \C-x заменяется на:
;; 1. C-x C-x - если выделен блок
;; 2. S-C-x во всех других случаях
;;
;(global-set-key [?\C-c-n-n] 'narrow-to-region) ; выделяем блок в обработку
;(global-set-key [?\C-c-n-w] 'widen)            ; возвращаемся в нормальный режим


;;
;; Toggle Artist Mode
;;  http://www.cinsk.org/emacs/emacs-artist.html
(define-key global-map "\C-xa" 'artist-mode)
; artis init
(add-hook
 'artist-mode-init-hook
 '(
   lambda()
     (define-key artist-mode-map "\C-s" 'artist-select-operation)
     (font-lock-mode 0)
  )
)
; artist deinit
(add-hook
  'artist-mode-exit-hook
  '(
    lambda()
      (font-lock-mode 1)
   )
)

;;
;; Switch windows
;;
; next window
(define-key global-map [f6]   '(lambda()
                                    (interactive)
                                    (select-window (next-window))))
; previous window
(define-key global-map [S-f6] '(lambda()
                                    (interactive)
                                    (select-window (previous-window))))
;(define-key global-map [C-S-up]    'windmove-up)
;(define-key global-map [C-S-down]  'windmove-down)
;(define-key global-map [C-S-left]  'windmove-left)
;(define-key global-map [C-S-right] 'windmove-right)


;;
;; Mouse context menu
;;
(global-set-key [(mouse-3)]       'mouse-major-mode-menu)
(global-set-key [menu]            'mouse-major-mode-menu)
(global-set-key [(shift mouse-3)] 'mouse-buffer-menu)

;
(provide 'hatred/keys-global-rc)