;;
;; ============================================================================================
;; Различные настройки для emhacks
;; (http://emhacks.sourceforge.net/)
;; В ArchLinux стоит поставить пакет emhacks из AUR
;; или установить из CVS
;; (обновлять из CVS: cvs update)
;; ============================================================================================
;;
; скачанное из CVS, если установленно из AUR просто закоментировать строчку
(add-to-list 'load-path "~/.emacs.d/lisp/emhacks")
; Панель вкладок
;
(setq EmacsPortable-global-tabbar 't) ; If you want tabbar
;(setq EmacsPortable-global-ruler 't) ; if you want a global ruler
;(setq EmacsPortable-popup-menu 't) ; If you want a popup menu.
;(setq EmacsPortable-popup-toolbar 't) ; If you want a popup toolbar
;;
(require 'tabbar-ruler)
;(require 'tabbar)
(tabbar-mode 't)
;
; Вариант решения задачи удаление ненужных буферов с табов
; http://www.emacswiki.org/emacs/TabBarMode#toc8
(setq tabbar-buffer-groups-function
           (lambda ()
             (list "All Buffers")))
(setq tabbar-buffer-list-function
        (lambda ()
          (remove-if
           (lambda(buffer)
             (find (aref (buffer-name buffer) 0) " *"))
           (buffer-list))))

;; Быстроее переключение между буферами
;; \H-\s - такую комбинацию даёт LeftWinKey
;;(global-set-key [?\C-\M-right] 'swbuff-switch-to-previous-buffer)
;;(global-set-key [?\C-\M-left] 'swbuff-switch-to-next-buffer)
;(global-set-key [M-left] 'swbuff-switch-to-previous-buffer)
;(global-set-key [M-right] 'swbuff-switch-to-next-buffer)
;;(defun swbuff-set-keys ()
;;   "Define keyboard shortcut \[M-right] for `swbuff-switch-to-next-buffer' and
;;                               [M-left] for `swbuff-switch-to-previous-buffe r'."
;;  (global-set-key [\M-right] 'swbuff-switch-to-next-buffer)
;;  (global-set-key [\M-left] 'swbuff-switch-to-previous-buffer))
;;(setq swbuff-load-hook '(swbuff-set-keys))
; Исключить системные буфера из переключения
(setq swbuff-exclude-buffer-regexps '("^ .*" "^\\*.*\\*" "TAGS"))
(require 'swbuff)
;
;; Утилита для отображения различий в файлах (Tools/Compate (GUI))
(require 'gdiff-setup)
;; Dir-tree
(require 'dir-tree)



;
(provide 'hatred/emhacks-rc)