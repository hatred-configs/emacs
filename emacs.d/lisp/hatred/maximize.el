;;
;; Maximize emacs on startup
;;
(defun maximize-emacs (&optional f)
  (interactive)
  ; for m$ windows
  (when (eq system-type 'windows-nt)
    (w32-send-sys-command 61488))
  ; for x window
  (when (eq system-type 'gnu/linux)
    (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
      '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
    (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
      '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))
  )

; add hook
; Needed only on old emacses
;(add-hook 'window-setup-hook 'maximize-emacs t)


(provide 'hatred/maximize)
