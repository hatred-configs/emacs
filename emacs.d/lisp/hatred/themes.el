;;
;; Color themes
;; Set it up via Options -> Customize Emacs -> Custom Themes
;; http://www.emacswiki.org/emacs/ColorTheme
;; http://dev.demas.me/?p=130
;; zenburn theme: https://raw.github.com/bbatsov/zenburn-emacs/master/zenburn-theme.el
;; tangotango theme: http://blog.nozav.org/post/2010/07/12/Updated-tangotango-emacs-color-theme
;;
(add-to-list 'custom-theme-load-path "~/.emacs.d/lisp/themes/")
(add-to-list 'load-path              "~/.emacs.d/lisp/themes/")
(require 'color-theme)
;(require 'color-theme-tango)
(setq color-theme-is-global t)
(color-theme-initialize)
;(color-theme-tango)
;(load-theme 'zenburn t)
; tangotango theme
;(require 'color-theme-tangotango)
;(color-theme-tangotango)



;
(provide 'hatred/themes)
